import { NgModule } from '@angular/core';
import { MapComponent } from './components/map/map.component';

@NgModule({
  declarations: [MapComponent],
  imports: [
  ],
  exports: [MapComponent]
})
export class NcMapModule { }
