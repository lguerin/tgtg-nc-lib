/**
 * Model permet d'afficher des coordonnées sur une Map
 */
export interface Coordinate {
  latitude: number;
  longitude: number;
}
