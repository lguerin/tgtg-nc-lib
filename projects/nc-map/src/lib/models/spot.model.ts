import { Coordinate } from './coordinate.model';

export interface Spot {
  text: string;
  coordinate: Coordinate;
}
