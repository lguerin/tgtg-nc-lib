import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import * as L from 'leaflet';
import { Coordinate } from '../../models/coordinate.model';
import { Spot } from '../../models/spot.model';

@Component({
  selector: 'nc-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit, OnDestroy {

  @Input()
  center: Coordinate;

  @Input()
  zoom = 13;

  @Input()
  spots: Array<Spot> = [];

  map: L.Map;

  constructor() {
    this.fixMarkerIcons();
  }

  ngOnInit(): void {
    this.initMap();
  }

  ngOnDestroy(): void {
    this.map.remove();
  }

  /**
   * Initialisation de la Map
   * @private
   */
  private initMap(): void {
    this.map = L.map('nc-map').setView([
      this.center.latitude,
      this.center.longitude
    ], this.zoom);

    const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 19,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });

    // Ajout des Markers
    this.addMarkersToMap();

    // Init du fond de carte
    tiles.addTo(this.map);
  }

  /**
   * Ajout des Markers sur la Map
   * @private
   */
  private addMarkersToMap(): void {
    if (this.spots.length > 0) {
      this.spots.forEach((s: Spot) => {
        L.marker([s.coordinate.latitude, s.coordinate.longitude])
          .addTo(this.map).bindPopup(s.text);
      });
    }
  }

  /**
   * Fixer le bug d'affichage de Leaflet pour référencer les URLs des markers
   * @private
   */
  private fixMarkerIcons(): void {
    const iconRetinaUrl = 'assets/marker-icon-2x.png';
    const iconUrl = 'assets/marker-icon.png';
    const shadowUrl = 'assets/marker-shadow.png';
    const iconDefault = L.icon({
      iconRetinaUrl,
      iconUrl,
      shadowUrl,
      iconSize: [25, 41],
      iconAnchor: [12, 41],
      popupAnchor: [1, -34],
      tooltipAnchor: [16, -28],
      shadowSize: [41, 41]
    });
    L.Marker.prototype.options.icon = iconDefault;
  }
}
