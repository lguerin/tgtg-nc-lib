import { Component, OnInit } from '@angular/core';
import { MapService } from '../../projects/nc-map/src/lib/services/map.service';
import { Coordinate } from '../../projects/nc-map/src/lib/models/coordinate.model';
import { Spot } from '../../projects/nc-map/src/lib/models/spot.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  title = 'tgtg-nc-lib';
  mapCenter: Coordinate;
  spots: Array<Spot> = [];

  constructor(private mapService: MapService) {
  }

  ngOnInit(): void {
    this.mapService.hello();
    const coordinate: Coordinate = { latitude: 48.900552, longitude: 2.25929};
    this.mapCenter = coordinate;
    const text = 'Exemple de marker';
    this.spots.push({ coordinate, text });
  }
}
