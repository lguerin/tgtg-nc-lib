import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NcMapModule } from '../../projects/nc-map/src/lib/nc-map.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NcMapModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
